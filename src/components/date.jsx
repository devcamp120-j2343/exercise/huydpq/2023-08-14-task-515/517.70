import { Component } from "react";

class DateTimeNow extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: "Hello world",
            dateTime: new Date().toLocaleString('en-GB', {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
                hour12: true
            }),
            color: null
        }
    }
    onChangeColor = () => {
        var seconds = new Date().getSeconds()
        console.log(seconds)

        if (seconds % 2 === 0) {
            this.setState({
                color: "red"
            })
        } else {
            this.setState({
                color: "blue"
            })
        }
    }

    render() {
        return (
            <>
                <h2 style={{ color: this.state.color }}>{this.state.text}</h2>
                <h3>It is {this.state.dateTime.toUpperCase()}</h3>
                <button onClick={this.onChangeColor}>Change Color</button>
            </>
        )
    }
}

export default DateTimeNow